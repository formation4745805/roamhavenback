-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 15 mai 2024 à 10:05
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `roamhaven`
--

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20240429123933', '2024-04-29 12:40:02', 1849),
('DoctrineMigrations\\Version20240429125306', '2024-04-29 12:53:20', 4019),
('DoctrineMigrations\\Version20240430141236', '2024-04-30 14:13:07', 273),
('DoctrineMigrations\\Version20240509071349', '2024-05-09 07:14:16', 432),
('DoctrineMigrations\\Version20240513132938', '2024-05-13 13:30:53', 1810);

-- --------------------------------------------------------

--
-- Structure de la table `messenger_messages`
--

DROP TABLE IF EXISTS `messenger_messages`;
CREATE TABLE IF NOT EXISTS `messenger_messages` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `available_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `delivered_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_75EA56E0FB7336F0` (`queue_name`),
  KEY `IDX_75EA56E0E3BD61CE` (`available_at`),
  KEY `IDX_75EA56E016BA31DB` (`delivered_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `roamhaven_categorie`
--

DROP TABLE IF EXISTS `roamhaven_categorie`;
CREATE TABLE IF NOT EXISTS `roamhaven_categorie` (
  `id` int NOT NULL AUTO_INCREMENT,
  `categorie_nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roamhaven_categorie`
--

INSERT INTO `roamhaven_categorie` (`id`, `categorie_nom`) VALUES
(1, 'Justice'),
(2, 'Gouvernement'),
(3, 'Shichibukai'),
(4, 'RoadTrip'),
(5, 'Samouraï'),
(6, 'Gastronomie'),
(7, 'Marché noir');

-- --------------------------------------------------------

--
-- Structure de la table `roamhaven_formulaire`
--

DROP TABLE IF EXISTS `roamhaven_formulaire`;
CREATE TABLE IF NOT EXISTS `roamhaven_formulaire` (
  `id` int NOT NULL AUTO_INCREMENT,
  `formulaire_message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `roamhaven_user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_699CD231EAD5EDAA` (`roamhaven_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `roamhaven_pays`
--

DROP TABLE IF EXISTS `roamhaven_pays`;
CREATE TABLE IF NOT EXISTS `roamhaven_pays` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pays_nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roamhaven_pays`
--

INSERT INTO `roamhaven_pays` (`id`, `pays_nom`) VALUES
(1, 'Enies Lobby'),
(2, 'Skypiea'),
(3, 'Amazon Lily'),
(4, 'Marine Ford'),
(5, 'Pays des Wa'),
(6, 'East blue'),
(7, 'West blue'),
(8, 'Ohara');

-- --------------------------------------------------------

--
-- Structure de la table `roamhaven_reservation`
--

DROP TABLE IF EXISTS `roamhaven_reservation`;
CREATE TABLE IF NOT EXISTS `roamhaven_reservation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `roamhaven_voyage_id` int DEFAULT NULL,
  `roamhaven_statut_id` int DEFAULT NULL,
  `roamhaven_formulaire_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CB29236695F320C` (`roamhaven_formulaire_id`),
  KEY `IDX_CB292366F1769F83` (`roamhaven_voyage_id`),
  KEY `IDX_CB2923666F9F4228` (`roamhaven_statut_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `roamhaven_statut`
--

DROP TABLE IF EXISTS `roamhaven_statut`;
CREATE TABLE IF NOT EXISTS `roamhaven_statut` (
  `id` int NOT NULL AUTO_INCREMENT,
  `statut_nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roamhaven_statut`
--

INSERT INTO `roamhaven_statut` (`id`, `statut_nom`) VALUES
(1, 'non lue'),
(2, 'en cours'),
(3, 'annulée'),
(4, 'acceptée');

-- --------------------------------------------------------

--
-- Structure de la table `roamhaven_user`
--

DROP TABLE IF EXISTS `roamhaven_user`;
CREATE TABLE IF NOT EXISTS `roamhaven_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_IDENTIFIER_USER_EMAIL` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roamhaven_user`
--

INSERT INTO `roamhaven_user` (`id`, `user_email`, `roles`, `password`, `user_nom`, `user_prenom`, `user_tel`, `is_verified`) VALUES
(2, 'rocheslaurent@gmail.com', '[\"ROLE_SUPER_ADMIN\", \"ROLE_USER\", \"ROLE_ADMIN\", \"ROLE_EDITEUR\"]', '$2y$13$DGvSm4f6SC.JcaG0pj4.5.eASKO5xkFSfZPQMKjQYYblsepyahxve', 'ROCHES', 'Laurent', '0102030405', 1),
(4, 'bob@bob.mer', '[]', '$2y$13$0HHWWQ1IKzTgLcm3vHc8pO6LVAJvezOw0L0cDcwnUZZZ.rCUgqnyq', 'Bob', 'L\'éponge', '6669996660', 0),
(5, 'rocheslaurent.professionnelle@gmail.com', '[\"ROLE_USER\", \"ROLE_ADMIN\"]', '$2y$13$Saoubatqz2D4BT.sXVCcwewx6/mCYf0TUx3EGsOM.2OIVqRoZyuRa', 'Employé', 'Modèle', '0504030201', 0),
(6, 'test@test.fr', '[\"ROLE_USER\"]', '$2y$13$l7MvhpSsy7QAJNaXUEtcv.nLvl3XO3izlw0DYoTTUOQNGuUHuu2bu', 'Test de génie', 'Edité', '001122334455', 0);

-- --------------------------------------------------------

--
-- Structure de la table `roamhaven_voyage`
--

DROP TABLE IF EXISTS `roamhaven_voyage`;
CREATE TABLE IF NOT EXISTS `roamhaven_voyage` (
  `id` int NOT NULL AUTO_INCREMENT,
  `voyage_nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voyage_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `voyage_debut` datetime NOT NULL,
  `voyage_image` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `voyage_duree` int NOT NULL,
  `roamhaven_user_id` int DEFAULT NULL,
  `voyage_prix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_36C2EE35EAD5EDAA` (`roamhaven_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roamhaven_voyage`
--

INSERT INTO `roamhaven_voyage` (`id`, `voyage_nom`, `voyage_description`, `voyage_debut`, `voyage_image`, `voyage_duree`, `roamhaven_user_id`, `voyage_prix`) VALUES
(1, 'Un week-end à Enies Lobby', 'Enies Lobby également appelée \"L\'Île Judiciaire\" (司法の島, Shihō no Shima); est une île appartenant au Gouvernement Mondial où les pirates de plus haut rang y sont amenés afin d\'y être jugés. Elle est entourée de vide pour l\'isoler complètement de toute tentative d\'intrusion. Il n\'y a qu\'un seul moyen d\'y parvenir, c\'est par le train des mers. L\'île est réputée imprenable d\'une part à cause de son isolement, d\'autre part parce qu\'elle est le QG du CP9', '2024-05-17 17:00:00', 'https://i.pinimg.com/originals/3b/25/28/3b2528bee9621ad9dbd4de88a2f98fb2.gif', 3, 2, '950'),
(2, 'Une semaine sur Amazon Lily', 'Amazon Lily est une île située dans Calm Belt. Grâce à cette mer où le vent et les tempêtes sont inexistantes et où les Rois des mers sont très nombreux, l\'île est très difficile d\'accès. Elle est couverte d\'une immense forêt, le point central de l\'île étant une montagne munie d\'un profond cratère, sur laquelle est sculptée de gigantesques statues de serpent et le nom de la tribu indigène de l\'île, Kuja, gravé en Kanji. Dans le cratère de la montagne se trouve le village de la tribu Kuja.', '2024-05-15 08:00:00', 'https://static1.cbrimages.com/wordpress/wp-content/uploads/2022/09/One-Piece-Amazon-Lily.jpg', 7, 2, '1500'),
(3, 'Escapade gouvernementale', 'Visitez les deux principales îles du gouvernement au travers de ce roadTrip d\'une semaine.', '2024-05-17 16:47:00', 'https://www.playcentral.de/wp-content/uploads/2023/09/800px-Kapitel552.png', 7, 5, '2500'),
(5, 'La tête dans les nuages', 'Avec ce RoadTrip vous monterez au ciel! Littéralement!!!\nAttention au coup de foudre avec Ener... Vous êtes prévenu!\nSkypiea (スカイピア, Sukaipia) est une île céleste située au dessus de Grand Line (à 10 000 mètres d\'altitude). C\'est le lieu principal où se déroule l\'Arc Skypiea.\nAprès l\'ellipse des deux ans, il y a une grande statue d\'Usopp à l\'entrée. C\'est la première île céleste à être mentionnée.', '2024-05-15 10:57:00', 'https://lh3.googleusercontent.com/cJPptxh7Qqtfx93W4KsFFRPZ8cV8JLf-RpoQ5g1Z1rDxChqMSHOlkY5soruGvea7WMEOWbi-XSiHdsMkwMVJLWu3ybYYEYsu0qOeA3mrcOOC8eVLO2XIS9sx38iQzkQEvMNbDguvjNDtRmh63H22ans', 3, 5, '1450'),
(13, 'Isolez-vous au Pays des Wa', 'Le Pays des Wa (ワノ国, Wano Kuni), ou Wano, aussi appelé anciennement \"Pays de l\'Or\" est un pays peuplé essentiellement de samouraïs et de ninjas, non affilié au Gouvernement Mondial. Il a été mentionné pour la première fois par Hogback, mais n\'est apparu que bien plus tard, lors de l\'Arc du même nom.\nL’île est située dans le Nouveau Monde. Autrefois dirigé pendent plusieurs siècles par la Famille Kozuki, le pays est actuellement occupé par L\'Équipage aux Cent Bêtes de l\'Empereur Kaido', '2024-05-21 15:42:00', 'https://staticg.sportskeeda.com/editor/2023/10/8fd54-16967760278696-1920.jpg', 7, 5, '2200'),
(14, 'Week-end gastronomique abordable', 'Le Baratie (バラティエ, Baratie) est un restaurant de haute mer, fondé et dirigé par Zeff. Il est situé dans la région de Sambas (サンバス 海域 Sanbasu Kaiiki) d\'East Blue. Sa cuisine raffinée ravira vos papilles!!!', '2024-05-21 10:54:00', 'https://ekladata.com/b7P7bfkbWKXwqex6HW2fygOzDDI@340x264.jpg', 3, 2, '750'),
(15, 'Cité mythique, passage incontournable!', 'Logue Town (ローグタウン, Rōgutaun) est une ville située sur une des îles d\'East Blue nommées les Îles Polestar (ポルスター諸島, Porusutā Shotō). La ville couvre presque toute l\'île, à part quelques collines.\n\nLogue Town est une ville majeure, car presque tous les navires, navires pirates compris, y font leurs provisions pour la Route de tous les Périls (Logue Town se situant proche de Reverse Mountain, hors de la vue de Red Line).', '2024-05-29 11:03:00', 'https://m.media-amazon.com/images/M/MV5BN2Y5YzQxNjEtYzJjNi00OTFiLWE4MzMtYmVjMDgxY2RlMTExXkEyXkFqcGdeQXVyNzgxMzc3OTc@._V1_.jpg', 3, 2, '1250'),
(16, 'Vestige des Lumière, preuve de l\'Obscurantisme', 'Ohara est une île située dans West Blue. Au centre de l’île se trouve un grand arbre nommé l\'arbre de la Cognition dont l\'intérieur est creux, contenant la célèbre bibliothèque d\'Ohara. L\'île se compose principalement d\'archéologues. C\'est également l\'île natale de Nico Robin. Elle a été complètement détruite par le Buster Call provoqué par Spandine membre du gouvernement mondial et père de Spandam.', '2024-05-27 11:11:00', 'https://lh3.googleusercontent.com/DhO4ixHtW3Q88-QyXQfDgvFexbJrI1rbEoNJZdtZIYR6wk0f8psTGoMK4k263VJ4yBcG7SiCJ2dd4TBF_KvcQqmTWa0HkYkd02DdboRVz-347Twd6jV84jdI1KmfrDi2h3_TfMiNx84eWYLuH5ND139mnvZO6NkFxb-NRn14R0Q5dDhmQAharTV3NFtePQ', 3, 5, '750'),
(17, 'Faites vous une peur bleue!!!', 'Thriller Bark (スリラーバーク, Surirā Bāku) est un bateau ressemblant à une île, situé dans le Triangle de Florian. Tel que révélé par Brook, ce bateau naviguait autrefois dans West Blue. c\'est le plus grand navire pirate au monde, appartenant au Capitaine Corsaire Gecko Moria. Il fut endommagé par les attaques de Moria et de Kuma lors de leurs combats contre L\'Équipage du Chapeau de Paille et, après la défaite de Moria, il fut laissé à l\'abandon.', '2024-05-29 11:13:00', 'https://static.wikia.nocookie.net/onepiece/images/0/01/Thriller_Bark.jpg/revision/latest/zoom-crop/width/500/height/500?cb=20100424071949&path-prefix=tr', 7, 5, '1500');

-- --------------------------------------------------------

--
-- Structure de la table `roamhaven_voyage_roamhaven_categorie`
--

DROP TABLE IF EXISTS `roamhaven_voyage_roamhaven_categorie`;
CREATE TABLE IF NOT EXISTS `roamhaven_voyage_roamhaven_categorie` (
  `roamhaven_voyage_id` int NOT NULL,
  `roamhaven_categorie_id` int NOT NULL,
  PRIMARY KEY (`roamhaven_voyage_id`,`roamhaven_categorie_id`),
  KEY `IDX_971A1BCEF1769F83` (`roamhaven_voyage_id`),
  KEY `IDX_971A1BCE3AC6751` (`roamhaven_categorie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roamhaven_voyage_roamhaven_categorie`
--

INSERT INTO `roamhaven_voyage_roamhaven_categorie` (`roamhaven_voyage_id`, `roamhaven_categorie_id`) VALUES
(1, 1),
(1, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 4),
(5, 4),
(8, 2),
(9, 5),
(10, 3),
(11, 2),
(12, 3),
(13, 5),
(14, 4),
(14, 6),
(15, 3),
(15, 4),
(16, 1),
(16, 2),
(17, 3),
(17, 7);

-- --------------------------------------------------------

--
-- Structure de la table `roamhaven_voyage_roamhaven_pays`
--

DROP TABLE IF EXISTS `roamhaven_voyage_roamhaven_pays`;
CREATE TABLE IF NOT EXISTS `roamhaven_voyage_roamhaven_pays` (
  `roamhaven_voyage_id` int NOT NULL,
  `roamhaven_pays_id` int NOT NULL,
  PRIMARY KEY (`roamhaven_voyage_id`,`roamhaven_pays_id`),
  KEY `IDX_1DB2E025F1769F83` (`roamhaven_voyage_id`),
  KEY `IDX_1DB2E025EB5F7C7B` (`roamhaven_pays_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roamhaven_voyage_roamhaven_pays`
--

INSERT INTO `roamhaven_voyage_roamhaven_pays` (`roamhaven_voyage_id`, `roamhaven_pays_id`) VALUES
(1, 1),
(2, 3),
(3, 1),
(3, 4),
(5, 2),
(12, 1),
(13, 5),
(14, 6),
(15, 6),
(16, 7),
(16, 8),
(17, 7);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `roamhaven_formulaire`
--
ALTER TABLE `roamhaven_formulaire`
  ADD CONSTRAINT `FK_699CD231EAD5EDAA` FOREIGN KEY (`roamhaven_user_id`) REFERENCES `roamhaven_user` (`id`);

--
-- Contraintes pour la table `roamhaven_reservation`
--
ALTER TABLE `roamhaven_reservation`
  ADD CONSTRAINT `FK_CB2923666F9F4228` FOREIGN KEY (`roamhaven_statut_id`) REFERENCES `roamhaven_statut` (`id`),
  ADD CONSTRAINT `FK_CB29236695F320C` FOREIGN KEY (`roamhaven_formulaire_id`) REFERENCES `roamhaven_formulaire` (`id`),
  ADD CONSTRAINT `FK_CB292366F1769F83` FOREIGN KEY (`roamhaven_voyage_id`) REFERENCES `roamhaven_voyage` (`id`);

--
-- Contraintes pour la table `roamhaven_voyage`
--
ALTER TABLE `roamhaven_voyage`
  ADD CONSTRAINT `FK_36C2EE35EAD5EDAA` FOREIGN KEY (`roamhaven_user_id`) REFERENCES `roamhaven_user` (`id`);

--
-- Contraintes pour la table `roamhaven_voyage_roamhaven_categorie`
--
ALTER TABLE `roamhaven_voyage_roamhaven_categorie`
  ADD CONSTRAINT `FK_971A1BCE3AC6751` FOREIGN KEY (`roamhaven_categorie_id`) REFERENCES `roamhaven_categorie` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_971A1BCEF1769F83` FOREIGN KEY (`roamhaven_voyage_id`) REFERENCES `roamhaven_voyage` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `roamhaven_voyage_roamhaven_pays`
--
ALTER TABLE `roamhaven_voyage_roamhaven_pays`
  ADD CONSTRAINT `FK_1DB2E025EB5F7C7B` FOREIGN KEY (`roamhaven_pays_id`) REFERENCES `roamhaven_pays` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_1DB2E025F1769F83` FOREIGN KEY (`roamhaven_voyage_id`) REFERENCES `roamhaven_voyage` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
