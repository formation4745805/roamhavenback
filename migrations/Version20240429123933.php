<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240429123933 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE roamhaven_categorie (id INT AUTO_INCREMENT NOT NULL, categorie_nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roamhaven_formulaire (id INT AUTO_INCREMENT NOT NULL, formulaire_message LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roamhaven_pays (id INT AUTO_INCREMENT NOT NULL, pays_nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roamhaven_reservation (id INT AUTO_INCREMENT NOT NULL, roamhaven_voyage_id INT DEFAULT NULL, INDEX IDX_CB292366F1769F83 (roamhaven_voyage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roamhaven_statut (id INT AUTO_INCREMENT NOT NULL, statut_nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roamhaven_user (id INT AUTO_INCREMENT NOT NULL, user_email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, user_nom VARCHAR(255) NOT NULL, user_prenom VARCHAR(255) NOT NULL, user_tel VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_IDENTIFIER_USER_EMAIL (user_email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roamhaven_voyage (id INT AUTO_INCREMENT NOT NULL, voyage_nom VARCHAR(255) NOT NULL, voyage_description LONGTEXT NOT NULL, voyage_debut DATETIME NOT NULL, voyage_image LONGTEXT NOT NULL, voyage_duree INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE roamhaven_reservation ADD CONSTRAINT FK_CB292366F1769F83 FOREIGN KEY (roamhaven_voyage_id) REFERENCES roamhaven_voyage (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE roamhaven_reservation DROP FOREIGN KEY FK_CB292366F1769F83');
        $this->addSql('DROP TABLE roamhaven_categorie');
        $this->addSql('DROP TABLE roamhaven_formulaire');
        $this->addSql('DROP TABLE roamhaven_pays');
        $this->addSql('DROP TABLE roamhaven_reservation');
        $this->addSql('DROP TABLE roamhaven_statut');
        $this->addSql('DROP TABLE roamhaven_user');
        $this->addSql('DROP TABLE roamhaven_voyage');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
