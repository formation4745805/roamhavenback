<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240429125306 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE roamhaven_pays_roamhaven_voyage (roamhaven_pays_id INT NOT NULL, roamhaven_voyage_id INT NOT NULL, INDEX IDX_E858D337EB5F7C7B (roamhaven_pays_id), INDEX IDX_E858D337F1769F83 (roamhaven_voyage_id), PRIMARY KEY(roamhaven_pays_id, roamhaven_voyage_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roamhaven_voyage_roamhaven_categorie (roamhaven_voyage_id INT NOT NULL, roamhaven_categorie_id INT NOT NULL, INDEX IDX_971A1BCEF1769F83 (roamhaven_voyage_id), INDEX IDX_971A1BCE3AC6751 (roamhaven_categorie_id), PRIMARY KEY(roamhaven_voyage_id, roamhaven_categorie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE roamhaven_pays_roamhaven_voyage ADD CONSTRAINT FK_E858D337EB5F7C7B FOREIGN KEY (roamhaven_pays_id) REFERENCES roamhaven_pays (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roamhaven_pays_roamhaven_voyage ADD CONSTRAINT FK_E858D337F1769F83 FOREIGN KEY (roamhaven_voyage_id) REFERENCES roamhaven_voyage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roamhaven_voyage_roamhaven_categorie ADD CONSTRAINT FK_971A1BCEF1769F83 FOREIGN KEY (roamhaven_voyage_id) REFERENCES roamhaven_voyage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roamhaven_voyage_roamhaven_categorie ADD CONSTRAINT FK_971A1BCE3AC6751 FOREIGN KEY (roamhaven_categorie_id) REFERENCES roamhaven_categorie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roamhaven_formulaire ADD roamhaven_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE roamhaven_formulaire ADD CONSTRAINT FK_699CD231EAD5EDAA FOREIGN KEY (roamhaven_user_id) REFERENCES roamhaven_user (id)');
        $this->addSql('CREATE INDEX IDX_699CD231EAD5EDAA ON roamhaven_formulaire (roamhaven_user_id)');
        $this->addSql('ALTER TABLE roamhaven_reservation ADD roamhaven_statut_id INT DEFAULT NULL, ADD roamhaven_formulaire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE roamhaven_reservation ADD CONSTRAINT FK_CB2923666F9F4228 FOREIGN KEY (roamhaven_statut_id) REFERENCES roamhaven_statut (id)');
        $this->addSql('ALTER TABLE roamhaven_reservation ADD CONSTRAINT FK_CB29236695F320C FOREIGN KEY (roamhaven_formulaire_id) REFERENCES roamhaven_formulaire (id)');
        $this->addSql('CREATE INDEX IDX_CB2923666F9F4228 ON roamhaven_reservation (roamhaven_statut_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CB29236695F320C ON roamhaven_reservation (roamhaven_formulaire_id)');
        $this->addSql('ALTER TABLE roamhaven_voyage ADD roamhaven_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE roamhaven_voyage ADD CONSTRAINT FK_36C2EE35EAD5EDAA FOREIGN KEY (roamhaven_user_id) REFERENCES roamhaven_user (id)');
        $this->addSql('CREATE INDEX IDX_36C2EE35EAD5EDAA ON roamhaven_voyage (roamhaven_user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE roamhaven_pays_roamhaven_voyage DROP FOREIGN KEY FK_E858D337EB5F7C7B');
        $this->addSql('ALTER TABLE roamhaven_pays_roamhaven_voyage DROP FOREIGN KEY FK_E858D337F1769F83');
        $this->addSql('ALTER TABLE roamhaven_voyage_roamhaven_categorie DROP FOREIGN KEY FK_971A1BCEF1769F83');
        $this->addSql('ALTER TABLE roamhaven_voyage_roamhaven_categorie DROP FOREIGN KEY FK_971A1BCE3AC6751');
        $this->addSql('DROP TABLE roamhaven_pays_roamhaven_voyage');
        $this->addSql('DROP TABLE roamhaven_voyage_roamhaven_categorie');
        $this->addSql('ALTER TABLE roamhaven_reservation DROP FOREIGN KEY FK_CB2923666F9F4228');
        $this->addSql('ALTER TABLE roamhaven_reservation DROP FOREIGN KEY FK_CB29236695F320C');
        $this->addSql('DROP INDEX IDX_CB2923666F9F4228 ON roamhaven_reservation');
        $this->addSql('DROP INDEX UNIQ_CB29236695F320C ON roamhaven_reservation');
        $this->addSql('ALTER TABLE roamhaven_reservation DROP roamhaven_statut_id, DROP roamhaven_formulaire_id');
        $this->addSql('ALTER TABLE roamhaven_formulaire DROP FOREIGN KEY FK_699CD231EAD5EDAA');
        $this->addSql('DROP INDEX IDX_699CD231EAD5EDAA ON roamhaven_formulaire');
        $this->addSql('ALTER TABLE roamhaven_formulaire DROP roamhaven_user_id');
        $this->addSql('ALTER TABLE roamhaven_voyage DROP FOREIGN KEY FK_36C2EE35EAD5EDAA');
        $this->addSql('DROP INDEX IDX_36C2EE35EAD5EDAA ON roamhaven_voyage');
        $this->addSql('ALTER TABLE roamhaven_voyage DROP roamhaven_user_id');
    }
}
