<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240513132938 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE roamhaven_voyage_roamhaven_pays (roamhaven_voyage_id INT NOT NULL, roamhaven_pays_id INT NOT NULL, INDEX IDX_1DB2E025F1769F83 (roamhaven_voyage_id), INDEX IDX_1DB2E025EB5F7C7B (roamhaven_pays_id), PRIMARY KEY(roamhaven_voyage_id, roamhaven_pays_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE roamhaven_voyage_roamhaven_pays ADD CONSTRAINT FK_1DB2E025F1769F83 FOREIGN KEY (roamhaven_voyage_id) REFERENCES roamhaven_voyage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roamhaven_voyage_roamhaven_pays ADD CONSTRAINT FK_1DB2E025EB5F7C7B FOREIGN KEY (roamhaven_pays_id) REFERENCES roamhaven_pays (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roamhaven_pays_roamhaven_voyage DROP FOREIGN KEY FK_E858D337EB5F7C7B');
        $this->addSql('ALTER TABLE roamhaven_pays_roamhaven_voyage DROP FOREIGN KEY FK_E858D337F1769F83');
        $this->addSql('DROP TABLE roamhaven_pays_roamhaven_voyage');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE roamhaven_pays_roamhaven_voyage (roamhaven_pays_id INT NOT NULL, roamhaven_voyage_id INT NOT NULL, INDEX IDX_E858D337EB5F7C7B (roamhaven_pays_id), INDEX IDX_E858D337F1769F83 (roamhaven_voyage_id), PRIMARY KEY(roamhaven_pays_id, roamhaven_voyage_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE roamhaven_pays_roamhaven_voyage ADD CONSTRAINT FK_E858D337EB5F7C7B FOREIGN KEY (roamhaven_pays_id) REFERENCES roamhaven_pays (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roamhaven_pays_roamhaven_voyage ADD CONSTRAINT FK_E858D337F1769F83 FOREIGN KEY (roamhaven_voyage_id) REFERENCES roamhaven_voyage (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roamhaven_voyage_roamhaven_pays DROP FOREIGN KEY FK_1DB2E025F1769F83');
        $this->addSql('ALTER TABLE roamhaven_voyage_roamhaven_pays DROP FOREIGN KEY FK_1DB2E025EB5F7C7B');
        $this->addSql('DROP TABLE roamhaven_voyage_roamhaven_pays');
    }
}
