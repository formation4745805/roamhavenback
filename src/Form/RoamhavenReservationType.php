<?php

namespace App\Form;

use App\Entity\RoamhavenReservation;
use App\Entity\RoamhavenStatut;
use App\Entity\roamhavenFormulaire;
use App\Entity\roamhavenVoyage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoamhavenReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('roamhavenVoyage', EntityType::class, [
                'class' => roamhavenVoyage::class,
'choice_label' => 'id',
            ])
            ->add('roamhavenStatut', EntityType::class, [
                'class' => RoamhavenStatut::class,
'choice_label' => 'id',
            ])
            ->add('roamhavenFormulaire', EntityType::class, [
                'class' => roamhavenFormulaire::class,
'choice_label' => 'id',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RoamhavenReservation::class,
        ]);
    }
}
