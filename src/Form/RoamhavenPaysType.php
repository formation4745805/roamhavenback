<?php

namespace App\Form;

use App\Entity\RoamhavenPays;
use App\Entity\roamhavenVoyage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoamhavenPaysType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('paysNom')
            // ->add('roamhavenVoyage', EntityType::class, [
            //     'class' => roamhavenVoyage::class,
            //     'choice_label' => 'id',
            //     'multiple' => true,
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RoamhavenPays::class,
        ]);
    }
}
