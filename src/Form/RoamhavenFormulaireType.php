<?php

namespace App\Form;

use App\Entity\RoamhavenFormulaire;
use App\Entity\roamhavenUser;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoamhavenFormulaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('formulaireMessage')
            ->add('roamhavenUser', EntityType::class, [
                'class' => roamhavenUser::class,
'choice_label' => 'id',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RoamhavenFormulaire::class,
        ]);
    }
}
