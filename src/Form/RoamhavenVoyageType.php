<?php

namespace App\Form;


use App\Entity\RoamhavenPays;
use App\Entity\RoamhavenVoyage;
use App\Entity\roamhavenCategorie;
use App\Entity\roamhavenUser;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoamhavenVoyageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('voyageNom')
            ->add('voyageDescription')
            ->add('voyageDebut', null, [
                'widget' => 'single_text'
            ])
            ->add('voyageImage')
            ->add('voyageDuree')
            ->add('voyagePrix')
            ->add('roamhavenPays', EntityType::class, [
                'class' => RoamhavenPays::class,
                'choice_label' => 'paysNom',
                'multiple' => true,
            ])
            ->add('roamhavenCategorie', EntityType::class, [
                'class' => roamhavenCategorie::class,
                'choice_label' => 'categorieNom',
                'multiple' => true,
            ])
            ->add('roamhavenUser', EntityType::class, [
                'class' => roamhavenUser::class,
                'choice_label' => 'userEmail',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RoamhavenVoyage::class,
        ]);
    }
}
