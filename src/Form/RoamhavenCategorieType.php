<?php

namespace App\Form;

use App\Entity\RoamhavenCategorie;
use App\Entity\RoamhavenVoyage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoamhavenCategorieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('categorieNom')
//             ->add('roamhavenVoyages', EntityType::class, [
//                 'class' => RoamhavenVoyage::class,
// 'choice_label' => 'id',
// 'multiple' => true,
//             ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RoamhavenCategorie::class,
        ]);
    }
}
