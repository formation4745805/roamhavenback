<?php

namespace App\Controller;

use App\Entity\RoamhavenCategorie;
use App\Form\RoamhavenCategorieType;
use App\Repository\RoamhavenCategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/categorie', name: 'app_categorie_')]
class CategorieController extends AbstractController
{
    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('s', name: 'index', methods: ['GET'])]
    public function index(RoamhavenCategorieRepository $roamhavenCategorieRepository): Response
    {
        return $this->render('categorie/index.html.twig', [
            'roamhaven_categories' => $roamhavenCategorieRepository->findAll(),
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $roamhavenCategorie = new RoamhavenCategorie();
        $form = $this->createForm(RoamhavenCategorieType::class, $roamhavenCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($roamhavenCategorie);
            $entityManager->flush();

            return $this->redirectToRoute('app_categorie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('categorie/new.html.twig', [
            'roamhaven_categorie' => $roamhavenCategorie,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(RoamhavenCategorie $roamhavenCategorie): Response
    {
        return $this->render('categorie/show.html.twig', [
            'roamhaven_categorie' => $roamhavenCategorie,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RoamhavenCategorie $roamhavenCategorie, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RoamhavenCategorieType::class, $roamhavenCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_categorie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('categorie/edit.html.twig', [
            'roamhaven_categorie' => $roamhavenCategorie,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, RoamhavenCategorie $roamhavenCategorie, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$roamhavenCategorie->getId(), $request->getPayload()->get('_token'))) {
            $entityManager->remove($roamhavenCategorie);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_categorie_index', [], Response::HTTP_SEE_OTHER);
    }
}
