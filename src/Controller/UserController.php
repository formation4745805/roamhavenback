<?php

namespace App\Controller;

use App\Entity\RoamhavenUser;
use App\Form\RoamhavenUserType;
use App\Repository\RoamhavenUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/user', name: 'app_user_')]
class UserController extends AbstractController
{
    #[IsGranted('ROLE_SUPER_ADMIN', message:"Tu n'es pas asez haut grader.")]
    #[Route('s', name: 'index', methods: ['GET'])]
    public function index(RoamhavenUserRepository $roamhavenUserRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'roamhaven_users' => $roamhavenUserRepository->findAll(),
        ]);
    }

    #[IsGranted('ROLE_SUPER_ADMIN', message:"Tu n'es pas asez haut grader.")]
    #[Route('/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $roamhavenUser = new RoamhavenUser();
        $form = $this->createForm(RoamhavenUserType::class, $roamhavenUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($roamhavenUser);
            $entityManager->flush();

            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('user/new.html.twig', [
            'roamhaven_user' => $roamhavenUser,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_USER', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(RoamhavenUser $roamhavenUser): Response
    {
        return $this->render('user/show.html.twig', [
            'roamhaven_user' => $roamhavenUser,
        ]);
    }

    #[IsGranted('ROLE_USER', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RoamhavenUser $roamhavenUser, EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasher): Response
    {
        $form = $this->createForm(RoamhavenUserType::class, $roamhavenUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $roamhavenUser->setPassword(
                $userPasswordHasher->hashPassword(
                $roamhavenUser,
                $form->get('password')->getData()
            )
        );
            $entityManager->flush();

            return $this->redirectToRoute('app_login', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('user/edit.html.twig', [
            'roamhaven_user' => $roamhavenUser,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_USER', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, RoamhavenUser $roamhavenUser, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$roamhavenUser->getId(), $request->getPayload()->get('_token'))) {
            $entityManager->remove($roamhavenUser);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_login', [], Response::HTTP_SEE_OTHER);
    }
}
