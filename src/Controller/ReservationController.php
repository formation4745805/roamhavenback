<?php

namespace App\Controller;

use App\Entity\RoamhavenReservation;
use App\Form\RoamhavenReservationType;
use App\Repository\RoamhavenReservationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/reservation', name:'app_reservation_')]
class ReservationController extends AbstractController
{
    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('s', name: 'index', methods: ['GET'])]
    public function index(RoamhavenReservationRepository $roamhavenReservationRepository): Response
    {
        return $this->render('reservation/index.html.twig', [
            'roamhaven_reservations' => $roamhavenReservationRepository->findAll(),
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $roamhavenReservation = new RoamhavenReservation();
        $form = $this->createForm(RoamhavenReservationType::class, $roamhavenReservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($roamhavenReservation);
            $entityManager->flush();

            return $this->redirectToRoute('app_reservation_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('reservation/new.html.twig', [
            'roamhaven_reservation' => $roamhavenReservation,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(RoamhavenReservation $roamhavenReservation): Response
    {
        return $this->render('reservation/show.html.twig', [
            'roamhaven_reservation' => $roamhavenReservation,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RoamhavenReservation $roamhavenReservation, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RoamhavenReservationType::class, $roamhavenReservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_reservation_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('reservation/edit.html.twig', [
            'roamhaven_reservation' => $roamhavenReservation,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, RoamhavenReservation $roamhavenReservation, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$roamhavenReservation->getId(), $request->getPayload()->get('_token'))) {
            $entityManager->remove($roamhavenReservation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_reservation_index', [], Response::HTTP_SEE_OTHER);
    }
}
