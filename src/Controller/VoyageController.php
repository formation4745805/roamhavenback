<?php

namespace App\Controller;

use App\Entity\RoamhavenUser;
use App\Entity\RoamhavenVoyage;
use App\Form\RoamhavenVoyageType;
use App\Repository\RoamhavenVoyageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/voyage', name: 'app_voyage_')]
class VoyageController extends AbstractController
{
    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('s', name: 'index', methods: ['GET'])]
    public function index(RoamhavenVoyageRepository $roamhavenVoyageRepository): Response
    {
        return $this->render('voyage/index.html.twig', [
            'roamhaven_voyages' => $roamhavenVoyageRepository->findAll(),
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('s/{id}', name: 'personnel', methods: ['GET'])]
    public function personnel(RoamhavenVoyageRepository $roamhavenVoyageRepository, RoamhavenUser $user): Response
    {
        return $this->render('voyage/personnel.html.twig', [
            'roamhaven_voyages' => $roamhavenVoyageRepository->findBy([
                'roamhavenUser' => $user
            ]),
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $roamhavenVoyage = new RoamhavenVoyage();
        $form = $this->createForm(RoamhavenVoyageType::class, $roamhavenVoyage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($roamhavenVoyage);
            $entityManager->flush();

            return $this->redirectToRoute('app_voyage_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('voyage/new.html.twig', [
            'roamhaven_voyage' => $roamhavenVoyage,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(RoamhavenVoyage $roamhavenVoyage): Response
    {
        return $this->render('voyage/show.html.twig', [
            'roamhaven_voyage' => $roamhavenVoyage,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RoamhavenVoyage $roamhavenVoyage, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RoamhavenVoyageType::class, $roamhavenVoyage);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_voyage_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('voyage/edit.html.twig', [
            'roamhaven_voyage' => $roamhavenVoyage,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, RoamhavenVoyage $roamhavenVoyage, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$roamhavenVoyage->getId(), $request->getPayload()->get('_token'))) {
            $entityManager->remove($roamhavenVoyage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_voyage_index', [], Response::HTTP_SEE_OTHER);
    }
}
