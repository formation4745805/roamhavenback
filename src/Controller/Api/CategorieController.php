<?php

namespace App\Controller\Api;

use App\Repository\RoamhavenCategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api/categorie', name: 'api_categorie_')]
class CategorieController extends AbstractController
{
    #[Route('/liste', name: 'liste')]
    public function liste(RoamhavenCategorieRepository $rhCategorieRepository): JsonResponse
    {
        $categorie = $rhCategorieRepository->findAll();
        return $this->json($categorie, context:[
            'groups' => 'api_voyage_miniature'
        ]);
    }
}
