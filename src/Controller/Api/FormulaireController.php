<?php

namespace App\Controller\Api;

use App\Entity\RoamhavenFormulaire;
use App\Entity\RoamhavenUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/formulaire', name: 'api_formulaire_')]
class FormulaireController extends AbstractController
{
    #[Route('/register', name: 'register')]
    public function new(SerializerInterface $serialize, ValidatorInterface $validator, EntityManagerInterface $em, Request $request): JsonResponse
    {
        $roamhavenUser = $serialize->deserialize($request->getContent(), RoamhavenUser::class, 'json', ['groups' => 'api_formulaire_register_user']);
        $roamhavenFormulaire = $serialize->deserialize($request->getContent(), RoamhavenFormulaire::class, 'json', ['groups' => 'api_formulaire_register_formulaire']);
        $errorsUser = $validator->validate($roamhavenUser);
        $errorsFormulaire = $validator->validate($roamhavenFormulaire);

        if($errorsUser->count()) {
            $messages = [];
            foreach ($errorsUser as $error) {
                $messages[] = $error->getMessage();
            }
                return $this->json($messages, Response::HTTP_UNPROCESSABLE_ENTITY);
        } else {
            $em->persist($roamhavenUser);
            $em->flush();
            if($errorsFormulaire->count()){
                $messages = [];
                foreach ($errorsUser as $error) {
                    $messages[] = $error->getMessage();
                }
                return $this->json($messages, Response::HTTP_UNPROCESSABLE_ENTITY);
            } else {
                $em->persist($roamhavenUser);
                $em->flush();
                return $this->json(null, Response::HTTP_CREATED);
            }
        }
    }

}
