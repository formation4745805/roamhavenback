<?php

namespace App\Controller\Api;

use App\Entity\RoamhavenVoyage;
use App\Repository\RoamhavenVoyageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api/voyage', name: 'api_voyage_')]
class VoyageController extends AbstractController
{
    #[Route('s', name: 'miniature')]
    public function miniature(RoamhavenVoyageRepository $rhVoyageRepository): JsonResponse
    {
        $voyages = $rhVoyageRepository->findAll();
        return $this->json($voyages, context:[
            'groups' => 'api_voyage_miniature'
        ]);
    }

    #[Route('/nom/{voyageNom}', name: 'show')]
    public function show(RoamhavenVoyage $rhVoyage): JsonResponse
    {
        return $this->json($rhVoyage, context:[
            'groups' => [
                'api_voyage_miniature',
                'api_voyage_show'
            ]
        ]);
    }

    #[Route('/id/{id}', name: 'details')]
    public function details(RoamhavenVoyage $rhVoyage): JsonResponse
    {
        return $this->json($rhVoyage, context:[
            'groups' => [
                'api_voyage_miniature',
                'api_voyage_show'
            ]
        ]);
    }

    #[Route('/pays/{roamhavenPays}', name: 'country')]
    public function country(EntityManagerInterface $em, $roamhavenPays): JsonResponse
    {
        $qb = $em->createQueryBuilder();
        $rhVoyage = $qb->select('v')
            ->from(RoamhavenVoyage::class, 'v')
            ->join('v.roamhavenPays', 'c')
            ->where('c.paysNom = :paysNom')
            ->setParameter('paysNom', $roamhavenPays)
            ->getQuery()
            ->getResult();
        return $this->json($rhVoyage, context:[
            'groups' => [
                'api_voyage_miniature',
            ]
        ]);
    }

    #[Route('/categorie/{roamhavenCategorie}', name: 'categorie')]
    public function categorie($roamhavenCategorie, EntityManagerInterface $em): JsonResponse
    {
        $qb = $em->createQueryBuilder();
        $rhVoyage = $qb->select('v')
            ->from(RoamhavenVoyage::class, 'v')
            ->join('v.roamhavenCategorie', 'c')
            ->where('c.categorieNom = :categorieNom')
            ->setParameter('categorieNom', $roamhavenCategorie)
            ->getQuery()
            ->getResult();
        return $this->json($rhVoyage, context:[
            'groups' => [
                'api_voyage_miniature',
            ]
        ]);
    }

    #[Route('/duree/{voyageDuree}', name: 'duree')]
    public function duree(RoamhavenVoyageRepository $rhVoyageRepository, $voyageDuree): JsonResponse
    {
        $rhVoyage = $rhVoyageRepository->findBy(['voyageDuree'=>$voyageDuree]);
        return $this->json($rhVoyage, context:[
            'groups' => [
                'api_voyage_miniature',
            ]
        ]);
    }
}
