<?php

namespace App\Controller\Api;

use App\Repository\RoamhavenPaysRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api/pays', name: 'api_pays_')]
class PaysController extends AbstractController
{
    #[Route('/liste', name: 'liste')]
    public function liste(RoamhavenPaysRepository $rhPaysRepository): JsonResponse
    {
        $pays = $rhPaysRepository->findAll();
        return $this->json($pays, context:[
            'groups' => 'api_voyage_miniature'
        ]);
    }
}
