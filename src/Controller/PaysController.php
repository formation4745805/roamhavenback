<?php

namespace App\Controller;

use App\Entity\RoamhavenPays;
use App\Form\RoamhavenPaysType;
use App\Repository\RoamhavenPaysRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/pays', name: 'app_pays_')]
class PaysController extends AbstractController
{
    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('liste', name: 'index', methods: ['GET'])]
    public function index(RoamhavenPaysRepository $roamhavenPaysRepository): Response
    {
        return $this->render('pays/index.html.twig', [
            'roamhaven_pays' => $roamhavenPaysRepository->findAll(),
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $roamhavenPays = new RoamhavenPays();
        $form = $this->createForm(RoamhavenPaysType::class, $roamhavenPays);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($roamhavenPays);
            $entityManager->flush();

            return $this->redirectToRoute('app_pays_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('pays/new.html.twig', [
            'roamhaven_pay' => $roamhavenPays,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(RoamhavenPays $roamhavenPays): Response
    {
        return $this->render('pays/show.html.twig', [
            'roamhaven_pays' => $roamhavenPays,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RoamhavenPays $roamhavenPays, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RoamhavenPaysType::class, $roamhavenPays);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_pays_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('pays/edit.html.twig', [
            'roamhaven_pays' => $roamhavenPays,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, RoamhavenPays $roamhavenPays, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$roamhavenPays->getId(), $request->getPayload()->get('_token'))) {
            $entityManager->remove($roamhavenPays);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_pays_index', [], Response::HTTP_SEE_OTHER);
    }
}
