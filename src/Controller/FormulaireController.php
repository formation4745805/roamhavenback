<?php

namespace App\Controller;

use App\Entity\RoamhavenFormulaire;
use App\Form\RoamhavenFormulaireType;
use App\Repository\RoamhavenFormulaireRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/formulaire', name: 'app_formulaire_')]
class FormulaireController extends AbstractController
{
    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('s', name: 'index', methods: ['GET'])]
    public function index(RoamhavenFormulaireRepository $roamhavenFormulaireRepository): Response
    {
        return $this->render('formulaire/index.html.twig', [
            'roamhaven_formulaires' => $roamhavenFormulaireRepository->findAll(),
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $roamhavenFormulaire = new RoamhavenFormulaire();
        $form = $this->createForm(RoamhavenFormulaireType::class, $roamhavenFormulaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($roamhavenFormulaire);
            $entityManager->flush();

            return $this->redirectToRoute('app_formulaire_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('formulaire/new.html.twig', [
            'roamhaven_formulaire' => $roamhavenFormulaire,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(RoamhavenFormulaire $roamhavenFormulaire): Response
    {
        return $this->render('formulaire/show.html.twig', [
            'roamhaven_formulaire' => $roamhavenFormulaire,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RoamhavenFormulaire $roamhavenFormulaire, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RoamhavenFormulaireType::class, $roamhavenFormulaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_formulaire_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('formulaire/edit.html.twig', [
            'roamhaven_formulaire' => $roamhavenFormulaire,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, RoamhavenFormulaire $roamhavenFormulaire, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$roamhavenFormulaire->getId(), $request->getPayload()->get('_token'))) {
            $entityManager->remove($roamhavenFormulaire);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_formulaire_index', [], Response::HTTP_SEE_OTHER);
    }
}
