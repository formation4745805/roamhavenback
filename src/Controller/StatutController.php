<?php

namespace App\Controller;

use App\Entity\RoamhavenStatut;
use App\Form\RoamhavenStatutType;
use App\Repository\RoamhavenStatutRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/statut', name: 'app_statut_')]
class StatutController extends AbstractController
{
    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('s', name: 'index', methods: ['GET'])]
    public function index(RoamhavenStatutRepository $roamhavenStatutRepository): Response
    {
        return $this->render('statut/index.html.twig', [
            'roamhaven_statuts' => $roamhavenStatutRepository->findAll(),
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $roamhavenStatut = new RoamhavenStatut();
        $form = $this->createForm(RoamhavenStatutType::class, $roamhavenStatut);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($roamhavenStatut);
            $entityManager->flush();

            return $this->redirectToRoute('app_statut_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('statut/new.html.twig', [
            'roamhaven_statut' => $roamhavenStatut,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(RoamhavenStatut $roamhavenStatut): Response
    {
        return $this->render('statut/show.html.twig', [
            'roamhaven_statut' => $roamhavenStatut,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RoamhavenStatut $roamhavenStatut, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RoamhavenStatutType::class, $roamhavenStatut);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_statut_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('statut/edit.html.twig', [
            'roamhaven_statut' => $roamhavenStatut,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN', message: "Tu n'as rien à faire là.")]
    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, RoamhavenStatut $roamhavenStatut, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$roamhavenStatut->getId(), $request->getPayload()->get('_token'))) {
            $entityManager->remove($roamhavenStatut);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_statut_index', [], Response::HTTP_SEE_OTHER);
    }
}
