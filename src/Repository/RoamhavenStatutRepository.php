<?php

namespace App\Repository;

use App\Entity\RoamhavenStatut;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RoamhavenStatut>
 *
 * @method RoamhavenStatut|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoamhavenStatut|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoamhavenStatut[]    findAll()
 * @method RoamhavenStatut[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoamhavenStatutRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoamhavenStatut::class);
    }

//    /**
//     * @return RoamhavenStatut[] Returns an array of RoamhavenStatut objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RoamhavenStatut
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
