<?php

namespace App\Repository;

use App\Entity\RoamhavenVoyage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RoamhavenVoyage>
 *
 * @method RoamhavenVoyage|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoamhavenVoyage|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoamhavenVoyage[]    findAll()
 * @method RoamhavenVoyage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoamhavenVoyageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoamhavenVoyage::class);
    }

//    /**
//     * @return RoamhavenVoyage[] Returns an array of RoamhavenVoyage objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RoamhavenVoyage
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
