<?php

namespace App\Repository;

use App\Entity\RoamhavenFormulaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RoamhavenFormulaire>
 *
 * @method RoamhavenFormulaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoamhavenFormulaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoamhavenFormulaire[]    findAll()
 * @method RoamhavenFormulaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoamhavenFormulaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoamhavenFormulaire::class);
    }

//    /**
//     * @return RoamhavenFormulaire[] Returns an array of RoamhavenFormulaire objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RoamhavenFormulaire
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
