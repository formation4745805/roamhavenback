<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\RoamhavenFormulaireRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NoSuspiciousCharacters;

#[ORM\Entity(repositoryClass: RoamhavenFormulaireRepository::class)]
class RoamhavenFormulaire
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message:"Un message est requis.")]
    #[Assert\Length(
        min:2, 
        minMessage:"Le message doit contenir au minimum 2 caractères", 
        max:500, 
        maxMessage:"Le messages ne peut pas dépasser 500 caractères."
    )]
    #[Assert\NoSuspiciousCharacters(
        checks: NoSuspiciousCharacters::CHECK_INVISIBLE, 
        restrictionLevel: NoSuspiciousCharacters::RESTRICTION_LEVEL_HIGH
    )]
    #[Groups([
        'api_formulaire_register_formulaire'
    ])]
    private ?string $formulaireMessage = null;

    #[ORM\ManyToOne(inversedBy: 'roamhavenFormulaires')]
    #[Groups([
        'api_formulaire_register_formulaire'
    ])]
    private ?roamhavenUser $roamhavenUser = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFormulaireMessage(): ?string
    {
        return $this->formulaireMessage;
    }

    public function setFormulaireMessage(string $formulaireMessage): static
    {
        $this->formulaireMessage = $formulaireMessage;

        return $this;
    }

    public function getRoamhavenUser(): ?roamhavenUser
    {
        return $this->roamhavenUser;
    }

    public function setRoamhavenUser(?roamhavenUser $roamhavenUser): static
    {
        $this->roamhavenUser = $roamhavenUser;

        return $this;
    }
}
