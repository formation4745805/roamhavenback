<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\RoamhavenPaysRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NoSuspiciousCharacters;

#[ORM\Entity(repositoryClass: RoamhavenPaysRepository::class)]
#[UniqueEntity('paysNom', message: "Ce pays est déjà enregistré.")]
class RoamhavenPays
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: "Un nom est requis.")]
    #[Assert\Length(
        min: 2,
        minMessage: "Le nom doit contenir au minimum 2 caractères",
        max: 255,
        maxMessage: "Le nom ne doit pas dépasser 255 caractères."
    )]
    #[Assert\NoSuspiciousCharacters(
        checks: NoSuspiciousCharacters::CHECK_INVISIBLE,
        restrictionLevel: NoSuspiciousCharacters::RESTRICTION_LEVEL_HIGH
    )]
    #[Groups('api_voyage_miniature')]
    private ?string $paysNom = null;

    /**
     * @var Collection<int, RoamhavenVoyage>
     */
    #[ORM\ManyToMany(targetEntity: RoamhavenVoyage::class, mappedBy: 'roamhavenPays')]
    private Collection $roamhavenVoyages;

    public function __construct()
    {
        $this->roamhavenVoyages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPaysNom(): ?string
    {
        return $this->paysNom;
    }

    public function setPaysNom(string $paysNom): static
    {
        $this->paysNom = $paysNom;

        return $this;
    }

    /**
     * @return Collection<int, RoamhavenVoyage>
     */
    public function getRoamhavenVoyages(): Collection
    {
        return $this->roamhavenVoyages;
    }

    public function addRoamhavenVoyage(RoamhavenVoyage $roamhavenVoyage): static
    {
        if (!$this->roamhavenVoyages->contains($roamhavenVoyage)) {
            $this->roamhavenVoyages->add($roamhavenVoyage);
            $roamhavenVoyage->addRoamhavenPays($this);
        }

        return $this;
    }

    public function removeRoamhavenVoyage(RoamhavenVoyage $roamhavenVoyage): static
    {
        if ($this->roamhavenVoyages->removeElement($roamhavenVoyage)) {
            $roamhavenVoyage->removeRoamhavenPays($this);
        }

        return $this;
    }
}
