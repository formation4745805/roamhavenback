<?php

namespace App\Entity;


use App\Repository\RoamhavenReservationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RoamhavenReservationRepository::class)]
class RoamhavenReservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne]
    private ?roamhavenVoyage $roamhavenVoyage = null;

    #[ORM\ManyToOne(inversedBy: 'roamhavenReservation')]
    private ?RoamhavenStatut $roamhavenStatut = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?roamhavenFormulaire $roamhavenFormulaire = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoamhavenVoyage(): ?roamhavenVoyage
    {
        return $this->roamhavenVoyage;
    }

    public function setRoamhavenVoyage(?roamhavenVoyage $roamhavenVoyage): static
    {
        $this->roamhavenVoyage = $roamhavenVoyage;

        return $this;
    }

    public function getRoamhavenStatut(): ?RoamhavenStatut
    {
        return $this->roamhavenStatut;
    }

    public function setRoamhavenStatut(?RoamhavenStatut $roamhavenStatut): static
    {
        $this->roamhavenStatut = $roamhavenStatut;

        return $this;
    }

    public function getRoamhavenFormulaire(): ?roamhavenFormulaire
    {
        return $this->roamhavenFormulaire;
    }

    public function setRoamhavenFormulaire(?roamhavenFormulaire $roamhavenFormulaire): static
    {
        $this->roamhavenFormulaire = $roamhavenFormulaire;

        return $this;
    }
}
