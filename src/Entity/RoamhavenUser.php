<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\RoamhavenUserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NoSuspiciousCharacters;
use Symfony\Component\Validator\Constraints\PasswordStrength;

#[ORM\Entity(repositoryClass: RoamhavenUserRepository::class)]
#[ORM\UniqueConstraint(name: 'UNIQ_IDENTIFIER_USER_EMAIL', fields: ['userEmail'])]
#[UniqueEntity(fields: ['userEmail'], message: 'Il existe déjà un compte avec cet Email: {{ value }} .')]
#[UniqueEntity('userTel', message: 'Il existe déjà un utilisateur avec ce numéro de téléphone: {{ value }} .')]
class RoamhavenUser implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([
        'api_formulaire_register_formulaire'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 180)]
    #[Assert\NotBlank(message:"L'Email est obligatoire.")]
    #[Assert\Email(
        message: 'The email {{ value }} is not a valid email.',
    )]
    #[Assert\Length(
        min:5, 
        minMessage:"Le nom doit contenir au minimum 5 caractères", 
        max:255, 
        maxMessage:"Le nom ne doit pas dépasser 255 caractères."
    )]
    #[Groups([
        'api_formulaire_register_user'
    ])]
    private ?string $userEmail = null;

    /**
     * @var list<string> The user roles
     */
    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    #[Assert\PasswordStrength([
        'minScore' => PasswordStrength::STRENGTH_MEDIUM, 
        'message' => "Le mot de passe n'est pas assez sûre!"
    ])]
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message:"Un nom est requis.")]
    #[Assert\Length(
        min:2, 
        minMessage:"Le nom doit contenir au minimum 2 caractères", 
        max:255, 
        maxMessage:"Le nom ne doit pas dépasser 255 caractères."
    )]
    #[Assert\NoSuspiciousCharacters(
        checks: NoSuspiciousCharacters::CHECK_INVISIBLE, 
        restrictionLevel: NoSuspiciousCharacters::RESTRICTION_LEVEL_HIGH
    )]
    #[Groups([
        'api_formulaire_register_user'
    ])]
    private ?string $userNom = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message:"Un prénom est requis.")]
    #[Assert\Length(
        min:2, 
        minMessage:"Le prénom doit contenir au minimum 2 caractères", 
        max:255, 
        maxMessage:"Le prénom ne doit pas dépasser 255 caractères."
    )]
    #[Assert\NoSuspiciousCharacters(
        checks: NoSuspiciousCharacters::CHECK_INVISIBLE, 
        restrictionLevel: NoSuspiciousCharacters::RESTRICTION_LEVEL_HIGH
    )]
    #[Groups([
        'api_formulaire_register_user'
    ])]
    private ?string $userPrenom = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message:"Un téléphone est requis.")]
    #[Assert\Length(
        min:10, 
        minMessage:"Le numéro doit contenir au minimum 10 caractères", 
        max:12, 
        maxMessage:"Le numéro ne doit pas dépasser 12 caractères."
    )]
    #[Assert\NoSuspiciousCharacters(
        checks: NoSuspiciousCharacters::CHECK_INVISIBLE, 
        restrictionLevel: NoSuspiciousCharacters::RESTRICTION_LEVEL_MODERATE
    )]
    #[Groups([
        'api_formulaire_register_user'
    ])]
    private ?string $userTel = null;

    /**
     * @var Collection<int, RoamhavenFormulaire>
     */
    #[ORM\OneToMany(targetEntity: RoamhavenFormulaire::class, mappedBy: 'roamhavenUser')]
    #[Groups([
        'api_formulaire_register_formulaire'
    ])]
    private Collection $roamhavenFormulaires;

    /**
     * @var Collection<int, RoamhavenVoyage>
     */
    #[ORM\OneToMany(targetEntity: RoamhavenVoyage::class, mappedBy: 'roamhavenUser')]
    private Collection $roamhavenVoyages;

    #[ORM\Column(type: 'boolean')]
    private $isVerified = false;

    public function __construct()
    {
        $this->roamhavenFormulaires = new ArrayCollection();
        $this->roamhavenVoyages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserEmail(): ?string
    {
        return $this->userEmail;
    }

    public function setUserEmail(string $userEmail): static
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->userEmail;
    }

    /**
     * @see UserInterface
     * @return list<string>
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param list<string> $roles
     */
    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserNom(): ?string
    {
        return $this->userNom;
    }

    public function setUserNom(string $userNom): static
    {
        $this->userNom = $userNom;

        return $this;
    }

    public function getUserPrenom(): ?string
    {
        return $this->userPrenom;
    }

    public function setUserPrenom(string $userPrenom): static
    {
        $this->userPrenom = $userPrenom;

        return $this;
    }

    public function getUserTel(): ?string
    {
        return $this->userTel;
    }

    public function setUserTel(string $userTel): static
    {
        $this->userTel = $userTel;

        return $this;
    }

    /**
     * @return Collection<int, RoamhavenFormulaire>
     */
    public function getRoamhavenFormulaires(): Collection
    {
        return $this->roamhavenFormulaires;
    }

    public function addRoamhavenFormulaire(RoamhavenFormulaire $roamhavenFormulaire): static
    {
        if (!$this->roamhavenFormulaires->contains($roamhavenFormulaire)) {
            $this->roamhavenFormulaires->add($roamhavenFormulaire);
            $roamhavenFormulaire->setRoamhavenUser($this);
        }

        return $this;
    }

    public function removeRoamhavenFormulaire(RoamhavenFormulaire $roamhavenFormulaire): static
    {
        if ($this->roamhavenFormulaires->removeElement($roamhavenFormulaire)) {
            // set the owning side to null (unless already changed)
            if ($roamhavenFormulaire->getRoamhavenUser() === $this) {
                $roamhavenFormulaire->setRoamhavenUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RoamhavenVoyage>
     */
    public function getRoamhavenVoyages(): Collection
    {
        return $this->roamhavenVoyages;
    }

    public function addRoamhavenVoyage(RoamhavenVoyage $roamhavenVoyage): static
    {
        if (!$this->roamhavenVoyages->contains($roamhavenVoyage)) {
            $this->roamhavenVoyages->add($roamhavenVoyage);
            $roamhavenVoyage->setRoamhavenUser($this);
        }

        return $this;
    }

    public function removeRoamhavenVoyage(RoamhavenVoyage $roamhavenVoyage): static
    {
        if ($this->roamhavenVoyages->removeElement($roamhavenVoyage)) {
            // set the owning side to null (unless already changed)
            if ($roamhavenVoyage->getRoamhavenUser() === $this) {
                $roamhavenVoyage->setRoamhavenUser(null);
            }
        }

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setVerified(bool $isVerified): static
    {
        $this->isVerified = $isVerified;

        return $this;
    }
}
