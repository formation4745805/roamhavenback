<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\RoamhavenStatutRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints\NoSuspiciousCharacters;

#[ORM\Entity(repositoryClass: RoamhavenStatutRepository::class)]
#[UniqueEntity('statutNom', message:"Ce statut existe déjà")]
class RoamhavenStatut
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message:"Un nom est requis.")]
    #[Assert\Length(
        min:2, 
        minMessage:"Le nom doit contenir au minimum 2 caractères", 
        max:255, 
        maxMessage:"Le nom ne doit pas dépasser 255 caractères."
    )]
    #[Assert\NoSuspiciousCharacters(
        checks: NoSuspiciousCharacters::CHECK_INVISIBLE, 
        restrictionLevel: NoSuspiciousCharacters::RESTRICTION_LEVEL_HIGH
    )]
    private ?string $statutNom = null;

    /**
     * @var Collection<int, roamhavenReservation>
     */
    #[ORM\OneToMany(targetEntity: roamhavenReservation::class, mappedBy: 'roamhavenStatut')]
    private Collection $roamhavenReservation;

    public function __construct()
    {
        $this->roamhavenReservation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatutNom(): ?string
    {
        return $this->statutNom;
    }

    public function setStatutNom(string $statutNom): static
    {
        $this->statutNom = $statutNom;

        return $this;
    }

    /**
     * @return Collection<int, roamhavenReservation>
     */
    public function getRoamhavenReservation(): Collection
    {
        return $this->roamhavenReservation;
    }

    public function addRoamhavenReservation(roamhavenReservation $roamhavenReservation): static
    {
        if (!$this->roamhavenReservation->contains($roamhavenReservation)) {
            $this->roamhavenReservation->add($roamhavenReservation);
            $roamhavenReservation->setRoamhavenStatut($this);
        }

        return $this;
    }

    public function removeRoamhavenReservation(roamhavenReservation $roamhavenReservation): static
    {
        if ($this->roamhavenReservation->removeElement($roamhavenReservation)) {
            // set the owning side to null (unless already changed)
            if ($roamhavenReservation->getRoamhavenStatut() === $this) {
                $roamhavenReservation->setRoamhavenStatut(null);
            }
        }

        return $this;
    }
}
