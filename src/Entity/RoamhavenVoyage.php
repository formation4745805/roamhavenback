<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\RoamhavenVoyageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NoSuspiciousCharacters;

#[ORM\Entity(repositoryClass: RoamhavenVoyageRepository::class)]
class RoamhavenVoyage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('api_voyage_miniature')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message:"Un nom est requis.")]
    #[Assert\Length(
        min:2, 
        minMessage:"Le nom doit contenir au minimum 2 caractères", 
        max:255, 
        maxMessage:"Le nom ne doit pas dépasser 255 caractères."
    )]
    #[Assert\NoSuspiciousCharacters(
        checks: NoSuspiciousCharacters::CHECK_INVISIBLE, 
        restrictionLevel: NoSuspiciousCharacters::RESTRICTION_LEVEL_HIGH
    )]
    #[Groups('api_voyage_miniature')]
    private ?string $voyageNom = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message:"Une description est requise.")]
    #[Assert\Length(
        min:2, 
        minMessage:"La description doit contenir au minimum 2 caractères", 
        max:500, 
        maxMessage:"La description ne doit pas dépasser 500 caractères."
    )]
    #[Assert\NoSuspiciousCharacters(
        checks: NoSuspiciousCharacters::CHECK_INVISIBLE, 
        restrictionLevel: NoSuspiciousCharacters::RESTRICTION_LEVEL_HIGH
    )]
    #[Groups('api_voyage_show')]
    private ?string $voyageDescription = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Assert\NotBlank(message:"Une date de début est requise.")]
    #[Groups('api_voyage_miniature')]
    private ?\DateTimeInterface $voyageDebut = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message:"Une image est requise.")]
    #[Assert\Url(message: "L'url {{ value }} n'est pas une url valide.")]
    #[Groups('api_voyage_miniature')]
    private ?string $voyageImage = null;

    #[ORM\Column]
    #[Assert\NotBlank(message:"Une durée est requise.")]
    #[Groups('api_voyage_miniature')]
    private ?int $voyageDuree = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message:"Un prix est requis.")]
    #[Groups('api_voyage_miniature')]
    private ?string $voyagePrix = null;

    /**
     * @var Collection<int, roamhavenCategorie>
     */
    #[ORM\ManyToMany(targetEntity: RoamhavenCategorie::class, inversedBy: 'roamhavenVoyages')]
    #[Groups('api_voyage_miniature')]
    private Collection $roamhavenCategorie;

    #[ORM\ManyToOne(inversedBy: 'roamhavenVoyages')]
    private ?roamhavenUser $roamhavenUser = null;

    /**
     * @var Collection<int, RoamhavenPays>
     */
    #[Groups('api_voyage_miniature')]
    #[ORM\ManyToMany(targetEntity: RoamhavenPays::class, inversedBy: 'roamhavenVoyages')]
    private Collection $roamhavenPays;


    public function __construct()
    {
        $this->roamhavenCategorie = new ArrayCollection();
        $this->roamhavenPays = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVoyageNom(): ?string
    {
        return $this->voyageNom;
    }

    public function setVoyageNom(string $voyageNom): static
    {
        $this->voyageNom = $voyageNom;

        return $this;
    }

    public function getVoyageDescription(): ?string
    {
        return $this->voyageDescription;
    }

    public function setVoyageDescription(string $voyageDescription): static
    {
        $this->voyageDescription = $voyageDescription;

        return $this;
    }

    public function getVoyageDebut(): ?\DateTimeInterface
    {
        return $this->voyageDebut;
    }

    public function setVoyageDebut(\DateTimeInterface $voyageDebut): static
    {
        $this->voyageDebut = $voyageDebut;

        return $this;
    }

    public function getVoyageImage(): ?string
    {
        return $this->voyageImage;
    }

    public function setVoyageImage(string $voyageImage): static
    {
        $this->voyageImage = $voyageImage;

        return $this;
    }

    public function getVoyageDuree(): ?int
    {
        return $this->voyageDuree;
    }

    public function setVoyageDuree(int $voyageDuree): static
    {
        $this->voyageDuree = $voyageDuree;

        return $this;
    }

    /**
     * @return Collection<int, roamhavenCategorie>
     */
    public function getRoamhavenCategorie(): Collection
    {
        return $this->roamhavenCategorie;
    }

    public function addRoamhavenCategorie(RoamhavenCategorie $roamhavenCategorie): static
    {
        if (!$this->roamhavenCategorie->contains($roamhavenCategorie)) {
            $this->roamhavenCategorie->add($roamhavenCategorie);
        }

        return $this;
    }

    public function removeRoamhavenCategorie(roamhavenCategorie $roamhavenCategorie): static
    {
        $this->roamhavenCategorie->removeElement($roamhavenCategorie);

        return $this;
    }

    public function getRoamhavenUser(): ?RoamhavenUser
    {
        return $this->roamhavenUser;
    }

    public function setRoamhavenUser(?RoamhavenUser $roamhavenUser): static
    {
        $this->roamhavenUser = $roamhavenUser;

        return $this;
    }

    public function getVoyagePrix(): ?string
    {
        return $this->voyagePrix;
    }

    public function setVoyagePrix(string $voyagePrix): static
    {
        $this->voyagePrix = $voyagePrix;

        return $this;
    }

    /**
     * @return Collection<int, RoamhavenPays>
     */
    public function getRoamhavenPays(): Collection
    {
        return $this->roamhavenPays;
    }

    public function addRoamhavenPays(RoamhavenPays $roamhavenPays): static
    {
        if (!$this->roamhavenPays->contains($roamhavenPays)) {
            $this->roamhavenPays->add($roamhavenPays);
        }

        return $this;
    }

    public function removeRoamhavenPays(RoamhavenPays $roamhavenPays): static
    {
        $this->roamhavenPays->removeElement($roamhavenPays);

        return $this;
    }
}
